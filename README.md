Please see the [freebsd-setup wiki](https://gitlab.com/duncan-bayne/freebsd-setup/wikis/home) for all documentation.

```
   _
  (_ _ _ _|_  _ _| __
  | | (-(-|_)_)(_|
   _ _|_    _
  _)(-|_|_||_)      __
           |   /|  | /|
                | .|/_|

```
