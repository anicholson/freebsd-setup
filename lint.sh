#!/bin/sh

set -e

shellcheck --version || pkg install -y hs-ShellCheck

find . -name '*.sh' | xargs shellcheck
