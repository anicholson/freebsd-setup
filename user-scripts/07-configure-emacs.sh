#!/bin/sh

set -e

git clone https://gitlab.com/duncan-bayne/emacs-setup ~/emacs-setup
ln -s ~/emacs-setup/emacs ~/.emacs
ln -s ~/emacs-setup/emacs.d ~/.emacs.d
