#!/bin/sh

set -e

rm -rf ~/.rbenv
git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
cp ~/.rbenv/completions/rbenv.zsh ~/.oh-my-zsh/custom/rbenv.zsh

export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

rbenv install 2.4.0
rbenv global 2.4.0
