#!/bin/sh

set -e

rm -f ~/.stumpwmrc
ln -s ~/freebsd-setup/conf/stumpwm/stumpwmrc ~/.stumpwmrc

echo exec stumpwm > ~/.xsession
echo exec stumpwm > ~/.xinitrc

chmod 0755 ~/.xsession ~/.xinitrc
