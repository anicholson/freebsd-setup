#!/bin/sh

set -e

echo >> /etc/rc.conf
echo \# Bluetooth settings, added by freebsd-setup >> /etc/rc.conf
echo hcsecd_enable=\"YES\" >> /etc/rc.conf
echo bthidd_enable=\"YES\" >> /etc/rc.conf
echo >> /etc/rc.conf

