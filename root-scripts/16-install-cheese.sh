#!/bin/sh

set -e

pkg install -y \
    cheese

kldload cuse

touch /boot/loader.conf
echo >> /boot/loader.conf
echo \# Webcam module, added by freebsd-setup >> /boot/loader.conf
echo cuse_load=\"YES\" >> /boot/loader.conf
echo >> /boot/loader.conf

touch /etc/rc.conf
echo >> /etc/rc.conf
echo \# Webcam services, added by freebsd-setup >> /etc/rc.conf
echo webcamd_enable=\"YES\" >> /etc/rc.conf
echo >> /etc/rc.conf

service devd restart


