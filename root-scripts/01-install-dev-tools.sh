#!/bin/sh

set -e

pkg install -y \
    autoconf \
    cabextract \
    ccrypt \
    cmake \
    cunit \
    curl \
    editors/emacs \
    freetds \
    gcc \
    git \
    gmake \
    ImageMagick \
    links \
    mercurial \
    mysql56-client \
    mysql56-server \
    rsync \
    ruby23 \
    shtool \
    subversion \
    sudo \
    the_silver_searcher \
    texinfo \
    v8 \
    wget \
    zenity \
    zsh
