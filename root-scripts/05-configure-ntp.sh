#!/bin/sh

set -e

echo >> /etc/rc.conf
echo \# NPT settings, added by freebsd-setup >> /etc/rc.conf
echo ntpd_enable=\"YES\" >> /etc/rc.conf
echo ntpdate_enable=\"YES\" >> /etc/rc.conf
echo >> /etc/rc.conf
