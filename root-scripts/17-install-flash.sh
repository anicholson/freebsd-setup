#!/bin/sh

set -e

kldload linux || echo Linux already loaded in kernel, proceeding ...

portsnap fetch update

portmaster www/linux-flashplayer
