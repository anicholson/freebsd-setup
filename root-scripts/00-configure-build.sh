#!/bin/sh

set -e

touch /etc/make.conf
echo >> /etc/make.conf
echo \# Custom build options, added by freebsd-setup >> /etc/make.conf
echo BATCH=yes >> /etc/make.conf
echo >> /etc/make.conf
