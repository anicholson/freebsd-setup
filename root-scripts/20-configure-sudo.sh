#!/bin/sh

set -e

echo "%wheel host = (root) NOPASSWD: /usr/sbin/acpiconf" > /usr/local/etc/sudoers.d/acpiconf

echo "%wheel host = (root) NOPASSWD: /usr/sbin/service" > /usr/local/etc/sudoers.d/service

echo "%wheel host = (root) NOPASSWD: /usr/sbin/halt" > /usr/local/etc/sudoers.d/halt

echo "%wheel host = (root) NOPASSWD: /sbin/reboot" > /usr/local/etc/sudoers.d/reboot

chown root:wheel /usr/local/etc/sudoers.d/*
chmod 0440 /usr/local/etc/sudoers.d/*

