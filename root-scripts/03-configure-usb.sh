#!/bin/sh

set -e

echo >> /etc/devfs.rules
echo \# USB access for non-root users, added by freebsd-setup >> /etc/devfs.rules
echo \# Thanks to http://mcuee.blogspot.com.au/2007/11/setting-up-permissions-for-usb-ports-to.html >> /etc/devfs.rules
echo [usb_devices=10] >> /etc/devfs.rules
echo add path \'ugen*\' mode 0660 group usb >> /etc/devfs.rules
echo add path \'da*s*\' mode 0660 group usb >> /etc/devfs.rules
echo >> /etc/devfs.rules

touch /etc/devfs.rules
touch /etc/devfs.conf
pw groupadd usb
pw group mod operator -m $USER
pw group mod wheel -m $USER

echo >> /etc/devfs.conf
echo \# USB access for non-root users, added by freebsd-setup >> /etc/devfs.conf
echo \# Thanks to http://mcuee.blogspot.com.au/2007/11/setting-up-permissions-for-usb-ports-to.html >> /etc/devfs.conf
echo perm usb0 0660 >> /etc/devfs.conf
echo own usb0 root:usb >> /etc/devfs.conf
echo perm usb1 0660 >> /etc/devfs.conf
echo own usb1 root:usb >> /etc/devfs.conf
echo >> /etc/devfs.conf

