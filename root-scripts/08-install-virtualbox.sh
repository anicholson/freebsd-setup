#!/bin/sh

set -e

pkg install -y \
    virtualbox-ose \
    virtualbox-ose-additions \
    virtualbox-ose-kmod

echo >> /etc/rc.conf
echo \# VirtualBox settings, added by freebsd-setup >> /etc/rc.conf
echo vboxnet_enable=\"YES\" >> /etc/rc.conf
echo >> /etc/rc.conf
