#!/bin/sh

set -e

pkg install -y \
    chromium \
    curl \
    docker-freebsd \
    firefox \
    gawk \
    gimp \
    libreoffice \
    p7zip \
    tmux \
    tree \
    vlc \
    xpdf
