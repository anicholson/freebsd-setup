#!/bin/sh

set -e

pkg install -y \
    scrot \
    unison \
    x11-drivers/xf86-input-keyboard \
    x11-drivers/xf86-input-mouse \
    xbrightness \
    xclip \
    xmixer \
    xorg \
    xscreensaver \
    xterm

touch /etc/rc.conf
echo >> /etc/rc.conf
echo \# Xorg and USB services, added by freebsd-setup >> /etc/rc.conf
echo hald_enable=\"YES\" >> /etc/rc.conf
echo dbus_enable=\"YES\" >> /etc/rc.conf
echo >> /etc/rc.conf

echo Desktop settings...
touch /etc/sysctl.conf
echo >> /etc/sysctl.conf
echo \# Enhanced desktop settings, some required by Chromium, added by freebsd-setup >> /etc/sysctl.conf
echo \# Thanks to https://cooltrainer.org/a-freebsd-desktop-howto/ >> /etc/sysctl.conf
echo kern.ipc.shmmax=67108864 >> /etc/sysctl.conf
echo kern.ipc.shmall=32768 >> /etc/sysctl.conf
echo kern.sched.preempt_thresh=224 >> /etc/sysctl.conf
echo kern.maxfiles=200000 >> /etc/sysctl.conf
echo kern.ipc.shm_allow_removed=1 >> /etc/sysctl.conf
echo vfs.usermount=1 >> /etc/sysctl.conf
echo >> /etc/sysctl.conf

