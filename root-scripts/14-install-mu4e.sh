#!/bin/sh

set -e

pkg install -y \
  isync \
  mail/mu4e \
  mu
