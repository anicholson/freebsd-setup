#!/bin/sh

set -e

echo >> /etc/rc.conf
echo \# Use custom rules, added by freebsd-setup >> /etc/rc.conf
echo devfs_system_ruleset="system" >> /etc/rc.con
